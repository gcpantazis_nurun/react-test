/** @jsx React.DOM */

'use strict';

// Shims for IE8
if (document.documentElement.className.match('lt-ie9')) {
  require('./ie8');
}

// Polyfills
require('WeakMap');
require('MutationObservers');
require('es5-shim');
require('es5-shim/es5-sham');

var $ = require('jquery');

var React = require('react');
var timer = require('./ui/Timer');
var LiveRegister = require('LiveRegister');

var todoapp = React.createClass({

  foo: function(){
    // Proof for the mutation observers.
    $(document.createElement('foo-bar')).insertAfter('foo-bar:last');
  },

  render: function() {
    return (
      <div onClick={this.foo}>
        <timer />
      </div>
    );
  }
});

LiveRegister('foo-bar', function(elm){
  React.renderComponent(<todoapp />, elm);
});
