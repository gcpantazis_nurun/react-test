'use strict';

var gulp = require('gulp');

// Load plugins
var $ = require('gulp-load-plugins')();

// Styles
gulp.task('styles', function() {
  return gulp.src('app/styles/main.scss')
    .pipe($.rubySass({
      style: 'expanded',
      precision: 10,
      loadPath: ['app/bower_components']
    }))
    .pipe($.autoprefixer('last 1 version'))
    .pipe(gulp.dest('dist/styles'))
    .pipe($.size())
    .pipe($.connect.reload());
});

var browserify = require('browserify');
var source = require('vinyl-source-stream');

gulp.task('scripts', function() {

  var bundler = browserify({
    entries: ['./app/scripts/app.js'],
    shim: {
      MutationObservers: {
        path: 'app/bower_components/MutationObservers/MutationObserver.js',
        exports: 'MutationObservers'
      }
    },
    debug: true,
    transform: ['reactify', 'debowerify']
  });

  return bundler
    .plugin('minifyify', {
      map: 'bundle.map.json',
      output: 'dist/scripts/bundle.map.json'
    })
    .bundle()
    .pipe(source('app.js'))
    .pipe(gulp.dest('dist/scripts'))
    .pipe($.connect.reload());

});

// HTML
gulp.task('html', function() {
  return gulp.src('app/*.html')
    .pipe($.useref())
    .pipe(gulp.dest('dist'))
    .pipe($.size())
    .pipe($.connect.reload());
});

// Images
gulp.task('images', function() {
  return gulp.src('app/images/**/*')
    .pipe($.cache($.imagemin({
      optimizationLevel: 3,
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest('dist/images'))
    .pipe($.size())
    .pipe($.connect.reload());
});

// Clean
gulp.task('clean', function() {
  return gulp.src(['dist'], {
    read: false
  }).pipe($.clean());
});

// Bundle
gulp.task('bundle', ['styles', 'scripts', 'bower'], function() {
  return gulp.src('./app/*.html')
    .pipe($.useref.assets())
    .pipe($.useref.restore())
    .pipe($.useref())
    .pipe(gulp.dest('dist'));
});

// Default task
gulp.task('default', ['clean'], function() {
  gulp.start('watch');
});

// Connect
gulp.task('connect', $.connect.server({
  root: ['dist'],
  port: 9000,
  livereload: true,
  middleware: function() {
    return [
      require('connect-gzip').gzip()
    ];
  }
}));

// Bower helper
gulp.task('bower', function() {
  gulp.src('app/bower_components/**/*.js', {
    base: 'app/bower_components'
  })
    .pipe(gulp.dest('dist/bower_components/'));

});

gulp.task('json', function() {
  gulp.src('app/scripts/json/**/*.json', {
    base: 'app/scripts'
  })
    .pipe(gulp.dest('dist/scripts/'));
});

// Watch
gulp.task('watch', ['html', 'bundle', 'connect'], function() {

  // Watch .json files
  gulp.watch('app/scripts/**/*.json', ['json']);

  // Watch .html files
  gulp.watch('app/*.html', ['html']);

  // Watch .scss files
  gulp.watch('app/styles/**/*.scss', ['styles']);

  // Watch .js files
  gulp.watch('app/scripts/**/*.js', ['scripts']);

  // Watch image files
  gulp.watch('app/images/**/*', ['images']);
});
